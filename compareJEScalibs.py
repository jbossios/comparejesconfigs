#					 #
# Author: Jona Bossio (jbossios@cern.ch) #
#					 #
# Date: December 14, 2018                #
#					 #
##########################################

from ROOT import *
import os,sys

# JES JetCalibTools' configs
Config_1 = "offline_lowjetpt_JES_constants.config"
Config_2 = "MC16a_MCJES_4EM_4LC_Oct2017.config"

# Jet Collections for each config
JetColl_1 = "offline_lowjetpt"
JetColl_2 = "AntiKt4EMTopo"

# Number of eta bins
nEtaBins = 90

# Legends for figures
Legend_1 = "Roberta"
Legend_2 = "Josef"

# Number of parameters of the energy response parametrization
nParameters = 9

# pT range in GeV
minPt = 0
maxPt = 2000

# Atlas Style
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

#############################
## DO NOT MODIFY
#############################

gROOT.SetBatch(True)  # so does not pop up plots!
SetAtlasStyle()

# Formatting instance of Plotter1D
Format = Plotter1D()
Format.SetATLASlabel("Internal")
Format.SetAxisTitles("E_{reco} [GeV]","Energy response")
Format.SetLogx()
Format.SetMoreLogLabelsX()
Format.SetComparisonType("Ratio")
Format.SetYTitleSecondPanel(Legend_1+"/"+Legend_2)
Format.SetLegendPosition("Bottom")
Format.SetReferenceSecondPanel(Legend_2)
Format.SetYRangeSecondPanel(0.9,1.1)

# Open Config files
File_1 = open(Config_1,"r")
File_2 = open(Config_2,"r")

# Get parameters
w, h = nParameters, nEtaBins;
Params_1 = [[0 for x in range(w)] for y in range(h)] 
Params_2 = [[0 for x in range(w)] for y in range(h)] 
for line in File_1:
  for ieta in range(0,nEtaBins):
    if "JES" in line and JetColl_1 in line and "Bin"+str(ieta)+":" in line:
      counter = 0
      for column in line.split():
        if "JES" in column: continue
        Params_1[ieta][counter] = column
	counter += 1
      # Protection
      if counter != nParameters:
        print "Number of parameters provided do not match the one provided, exiting"
        sys.exit(0)
for line in File_2:
  for ieta in range(0,nEtaBins):	
    if "JES" in line and JetColl_2 in line and "Bin"+str(ieta)+":" in line:
      counter = 0	  
      for column in line.split():
        if "JES" in column: continue
        Params_2[ieta][counter] = column
        counter += 1
      # Protection	
      if counter != nParameters:
        print "Number of parameters provided do not match the one provided, exiting"
        sys.exit(0)

# Loop over eta bins
minEta = -4.5
maxEta = -4.4
for ieta in range(0,nEtaBins):
  # Create a TF1 for each config file
  Expression = "[0]"
  for iparam in range(1,nParameters): 
    Expression += "+["+str(iparam)+"]*TMath::Power(log(x),"+str(iparam)+")"
  TF1_1 = TF1("F1",Expression,minPt,maxPt)
  TF1_2 = TF1("F2",Expression,minPt,maxPt)
  for iparam in range(0,nParameters):
    TF1_1.SetParameter(int(iparam),Double(Params_1[ieta][iparam]))
    counter += 1
  for iparam in range(0,nParameters):
    TF1_2.SetParameter(int(iparam),Double(Params_2[ieta][iparam]))
    counter += 1
  # Compare calibrations
  if ieta < 10:
    Plot = Plotter1D(Legend_1+"_vs_"+Legend_2+"_Bin0"+str(ieta))
  else:
    Plot = Plotter1D(Legend_1+"_vs_"+Legend_2+"_Bin"+str(ieta))
  Plot.ApplyFormat(Format)
  Plot.AddHist(Legend_2,TF1_2.CreateHistogram(),"HIST][")
  Plot.AddHist(Legend_1,TF1_1.CreateHistogram(),"HIST][")
  if minEta < 0.01 and minEta > 0: minEta = 0
  if maxEta < 0.01 and maxEta > 0: maxEta = 0
  Plot.AddText(0.2,0.75,str(minEta)+"<#eta<"+str(maxEta))
  Plot.Write()
  minEta += 0.1 
  maxEta += 0.1

print ">>> DONE <<<"




